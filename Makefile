.PHONEY: fresh clean

css/bootstrap.css: less/bootstrap.less
	lessc less/bootstrap.less > css/bootstrap.css

fresh: clean css/bootstrap.css

clean:
	rm -f css/bootstrap.css